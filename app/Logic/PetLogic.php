<?php

namespace App\Logic;

use App\Models\Pet;

class PetLogic extends Pet
{
    public static function createFromArray(array $data)
    {
        $pet = self::create($data);

        // Handle the relationships
        if (isset($data['category'])) {
            $category = CategoryLogic::firstOrCreate($data['category']);
            $pet->category()->associate($category);
        }

        if (isset($data['photoUrls'])) {
            $photos = PhotoUrlLogic::mapPhotoUrlsFromArray($data['photoUrls'], true);
            $pet->photoUrls()->createMany($photos);
        }

        if (isset($data['tags'])) {
            $tags = TagLogic::findOrCreateMany($data['tags']);
            $tags->each(function ($tag) use ($pet) {
                $pet->tags()->attach($tag);
            });
        }

        $pet->save();
        return $pet;
    }
}
