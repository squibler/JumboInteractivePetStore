<?php

namespace App\Logic;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagLogic extends Tag
{
    public static function findOrCreateMany(array $tags)
    {
        $tags = collect($tags);
        $mapped = $tags->map(function ($tag) {
            $tag['name'] = strtolower($tag['name']);
            return self::firstOrCreate($tag);
        });
        return $mapped->unique('name');
    }
}
