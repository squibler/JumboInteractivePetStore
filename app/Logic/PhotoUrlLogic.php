<?php

namespace App\Logic;

use App\Models\PhotoUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PhotoUrlLogic extends PhotoUrl
{
    public static function mapPhotoUrlsFromArray(array $photos, $assoc = false)
    {
        $urls = collect($photos);
        $urls = $urls->map(function ($url) {
            return ['url' => $url];
        });

        return ($assoc) ? $urls->toArray() : $urls;
    }
}
