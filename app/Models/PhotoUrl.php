<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Logic\PetLogic;

class PhotoUrl extends Model
{
    protected $table = 'photos';

    protected $fillable = ['url'];

    public function pet()
    {
        return $this->belongsTo(PetLogic::class, 'pet_id', 'id');
    }
}
