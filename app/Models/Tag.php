<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Logic\PetLogic;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = [
        'name'
    ];

    public function pets()
    {
        return $this->belongsToMany(PetLogic::class, 'pets_tags', 'tag_id', 'pet_id');
    }
}
