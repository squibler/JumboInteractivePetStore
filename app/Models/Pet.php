<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Logic\CategoryLogic;
use App\Logic\PhotoUrlLogic;
use App\Logic\TagLogic;

class Pet extends Model
{
    use SoftDeletes;

    protected $table = 'pets';

    protected $fillable = [
        'name', 'status'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $with = [
        'category'
    ];

    public function category()
    {
        return $this->belongsTo(CategoryLogic::class);
    }


    public function photoUrls()
    {
        return $this->hasMany(PhotoUrlLogic::class, 'pet_id');
    }


    public function tags()
    {
        return $this->belongsToMany(TagLogic::class, 'pets_tags', 'pet_id', 'tag_id');
    }
}
