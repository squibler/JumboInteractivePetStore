<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Logic\PetLogic;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'name'
    ];

    public function pets()
    {
        return $this->hasMany(PetLogic::class, 'category_id');
    }
}
