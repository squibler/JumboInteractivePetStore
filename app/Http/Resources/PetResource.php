<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'category' => new CategoryResource($this->category),
            'photUrls' => PhotoUrlResource::collection($this->photoUrls),
            'tags' => TagResource::collection($this->tags)
        ];
    }
}
