<?php

namespace App\Http\Controllers;

use App\Logic\PetLogic;
use App\Http\Requests\StorePetRequest;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\PetResource;

/**
 * Class PetController
 *
 * @package App\Http\Controllers
 */
class PetController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePetRequest $request
     * @return JsonResponse
     */
    public function store(StorePetRequest $request)
    {
        return new PetResource(
            PetLogic::createFromArray($request->validated())
        );
    }

    public function list()
    {
        return PetResource::collection(PetLogic::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  PetLogic $pet
     * @return Responsable
     */
    public function show(PetLogic $pet)
    {
        return new PetResource($pet);
    }

    /**
     * Remove this pet from storage.
     *
     * @param  PetLogic $pet
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(PetLogic $pet)
    {
        if ($pet->delete()) {
            return response('success', 204);
        }
        return response('error', 404);
    }
}
