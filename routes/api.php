<?php

use Illuminate\Http\Request;

Route::post('/pet', 'PetController@store');
Route::get('/pets', 'PetController@list');
Route::get('/pet/{pet}', 'PetController@show');
Route::put('/pet/{pet}', 'PetController@update');
Route::delete('/pet/{pet}', 'PetController@destroy');
