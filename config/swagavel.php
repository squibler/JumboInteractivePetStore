<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Swagger File
    |--------------------------------------------------------------------------
    |
    | This value is the relative path to the swagger file
    | in your application.
    |
    */

    'file' => env('SWAGAVEL_FILE', './petstore.v3.yaml'),

    /*
    |--------------------------------------------------------------------------
    | Strict Mode
    |--------------------------------------------------------------------------
    |
    | Strict mode will crash swagavel at the first sign of trouble,
    | leave false unless debugging.
    |
    */

    'strict' => env('SWAGAVEL_STRICT', false),

    /*
    |--------------------------------------------------------------------------
    | Active Generators
    |--------------------------------------------------------------------------
    |
    | The generators listed here will be loaded when swagavel runs.
    | By default all generators are used but feel free to add
    | custom generators or disable default ones.
    |
    */

    'generators' => [
        \RexSoftware\Swagavel\Generators\ControllerGenerator::class,
        \RexSoftware\Swagavel\Generators\ControllerTestGenerator::class,
        \RexSoftware\Swagavel\Generators\MigrationGenerator::class,
        \RexSoftware\Swagavel\Generators\ModelFactoryGenerator::class,
        \RexSoftware\Swagavel\Generators\ModelGenerator::class,
        \RexSoftware\Swagavel\Generators\RequestGenerator::class,
        \RexSoftware\Swagavel\Generators\RouteGenerator::class,
        \RexSoftware\Swagavel\Generators\TableSeederGenerator::class,
        \RexSoftware\Swagavel\Generators\TransformerGenerator::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Models Namespace
    |--------------------------------------------------------------------------
    |
    | The namespace for models in project. Some generators will require
    | this value produce correct code.
    |
    */

    'models_namespace' => 'App\\Models',

    /*
    |--------------------------------------------------------------------------
    | Routes Path
    |--------------------------------------------------------------------------
    |
    | Path to routes file. Some generators will require this value to
    | know where to write their code.
    |
    */

    'routes_path' => 'routes/api.php',
];
