<?php

namespace Tests\Support;

use Illuminate\Support\Facades\File;

trait LoadAssets
{
    protected static $loadedAssets = [];

    protected function loadAsset(string $asset)
    {
        $index = md5($asset);
        if (! isset(static::$loadedAssets[$index])) {
            $path = realpath(__DIR__ . '/../Assets/' . $asset);
            $this->assertFileExists($path);
            static::$loadedAssets[$index] = File::get($path);
        }

        return static::$loadedAssets[$index];
    }


    protected function loadJsonAsset(string $asset, array $replacements = null, $returnObject = false)
    {
        $this->assertStringEndsWith('json', $asset);
        $template = $this->loadAsset($asset);

        if ($replacements) {
            $template = str_replace(
                array_keys($replacements),
                array_values($replacements),
                $template
            );
        }

        return json_decode($template, !$returnObject);
    }
}
