<?php

namespace Tests\Support;

trait UriBuilder
{
    protected function apiPrefix(string $uri)
    {
        static $apiPrefix = '';
        if (! $apiPrefix) {
            $env = env('API_DEFAULT_PREFIX', 'api');
            $apiPrefix = '/' . trim($env, '/');
        }
        return $apiPrefix . $uri;
    }


    protected function uri(string $endpoint = null, array $params = null, $isApi = false)
    {
        $uri = (($endpoint) ? '/' . trim($endpoint, '/') : '');
        if ($params) {
            $uri .= '?' . http_build_query($params);
        }
        return ($isApi) ? $this->apiPrefix($uri) : $uri;
    }


    protected function apiUri(string $endpoint = null, array $params = null)
    {
        return $this->uri($endpoint, $params, true);
    }
}
