<?php

namespace Tests\Support;

trait TestHeaders
{
    protected $jsonHeaders = [
        'X-Requested-With'  => 'XMLHttpRequest',
        'Content-Type'      => 'application/json',
    ];


    protected function headers(array $headers = [])
    {
        return array_merge($this->jsonHeaders, $headers);
    }
}
