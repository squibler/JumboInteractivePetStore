<?php

namespace Tests\Support;

/**
 * Class CalendarControllerTest
 *
 * @package Tests\Feature
 */
trait InjectHeaders
{
    use TestHeaders;
    /**
     * The rest of the methods are there to override
     * some of the parent test methods to inject the application's
     * required headers
     *
     * @param  array $headers
     * @return void
     */
    public function get($uri, array $headers = [])
    {
        return parent::get($uri, $this->headers($headers));
    }


    public function json($method, $uri, array $data = [], array $headers = [])
    {
        return parent::json($method, $uri, $data, $this->headers($headers));
    }


    public function getJson($uri, array $headers = [])
    {
        return parent::getJson($uri, $this->headers($headers));
    }


    public function postJson($uri, array $data = [], array $headers = [])
    {
        return parent::postJson($uri, $data, $this->headers($headers));
    }


    public function patchJson($uri, array $data = [], array $headers = [])
    {
        return parent::patchJson($uri, $data, $this->headers($headers));
    }


    public function deleteJson($uri, array $data = [], array $headers = [])
    {
        return parent::deleteJson($uri, $data, $this->headers($headers));
    }
}
