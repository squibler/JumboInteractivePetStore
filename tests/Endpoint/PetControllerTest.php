<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;

/**
 * Class PetControllerTest
 *
 * @package Tests\Feature
 */
class PetControllerTest extends TestCase
{
    // use DatabaseMigrations;

    protected static $pet;

    /**
     * Store a newly created resource in storage.
     *
     * @test
     */
    public function storesAPet()
    {
        $uri = $this->apiUri('pet');
        $payload = $this->loadJsonAsset('pet.json', [
            '{{PET_NAME}}' => 'Pat',
            '{{CATEGORY_ID}}' => '1',
            '{{CATEGORY_NAME}}' => 'dog',
            '{{TAG_ID}}' => '1',
            '{{TAG_NAME1}}' => 'terrier',
            '{{TAG_NAME2}}' => 'smells bad'
        ]);
        $this->assertDatabaseMissing('pets', ['name' => 'Pat']);
        $response = $this->postJson($uri, $payload);
        $response->assertStatus(201);
        $this->assertDatabaseHas('pets', ['name' => 'Pat']);
        $this->assertDatabaseHas('categories', ['name' => 'dog']);
        $this->assertDatabaseHas('photos', ['url' => 'http://example.com/picture-1.jpg']);

        static::$pet = ($response->getData())->data;
    }

    /**
     * Display the specified resource.
     *
     * @test
     * @depends storesAPet
     */
    public function listAllAvailablePets()
    {
        $uri = $this->apiUri('pets');
        $response = $this->getJson($uri);

        $response->assertJsonStructure([
            'data' => [
                '*' => ['id', 'name', 'status']
            ]
        ]);

        $this->assertResponseStatus($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @test
     * @depends storesAPet
     */
    public function show()
    {
        $pet = static::$pet;
        $uri = $this->apiUri('pet/'. $pet->id);
        $response = $this->getJson($uri);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'status'
            ]
        ]);
        $this->assertResponseStatus($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @test
     * @depends storesAPet
     */
    public function destroy()
    {
        $pet = static::$pet;
        $uri = $this->apiUri('/pet/'.$pet->id);
        $response = $this->deleteJson($uri);

        $this->assertResponseStatus($response, 204);
        $this->assertSoftDeleted('pets', ['name' => $pet->name]);
    }
}
