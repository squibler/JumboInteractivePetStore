<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Logic\PhotoUrlLogic;
use Illuminate\Support\Collection;

class PhotoUrlLogicTest extends TestCase
{
    protected $testdata = [
        'http://example.org/1.jpeg',
        'http://example.org/2.jpeg',
        'http://example.org/3.jpeg',
        'http://example.org/4.jpeg',
        'http://example.org/5.jpeg'
    ];

    /**
     * @return void
     * @test
     */
    public function mapPhotoUrlsFromArrayReturnsCollectionObjectWhenGivenArray()
    {
        $photos = PhotoUrlLogic::mapPhotoUrlsFromArray($this->testdata);
        $this->assertInstanceOf(Collection::class, $photos);
        $this->assertSame($this->testdata[0], $photos->first()['url']);
    }

    /**
     * @return void
     * @test
     */
    public function mapPhotoUrlsFromArrayReturnsArrayWhenGivenArrayAndAssocDefined()
    {
        $photos = PhotoUrlLogic::mapPhotoUrlsFromArray($this->testdata, true);
        $this->assertIsArray($photos);
        $this->assertArrayHasKey('url', $photos[0]);
        $this->assertSame($this->testdata[0], $photos[0]['url']);
    }
}
