<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Logic\TagLogic;
use Illuminate\Support\Collection;

class TagLogicTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function itShouldFindOrCreateTagsWhenGivenAnArray()
    {
        $tags = TagLogic::findOrCreateMany([
            ['name' => 'hello'],
            ['name' => 'world'],
            ['id' => 1, 'name' => 'HELLO']
        ]);

        $this->assertInstanceOf(Collection::class, $tags);

        // Event though we have 3 tags in the dataset, it should only return 2 because
        // two of the test data tags are the same except for string case
        $this->assertCount(2, $tags);
        $this->assertDatabaseHas('tags', ['name' => 'hello']);
        $this->assertDatabaseHas('tags', ['name' => 'world']);
    }
}
