<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\StorePetRequest;
use App\Logic\PetLogic;

class PetLogicTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function itShouldCreateAPetFromAnArray()
    {
        $data = $this->loadJsonAsset('pet.json', [
            '{{PET_NAME}}' => 'Whiskers',
            '{{CATEGORY_ID}}' => '2',
            '{{CATEGORY_NAME}}' => 'cat',
            '{{TAG_ID}}' => '9',
            '{{TAG_NAME1}}' => 'persian',
            '{{TAG_NAME2}}' => 'hates humans'
        ]);

        $pet = PetLogic::createFromArray($data);
        $this->assertInstanceOf(PetLogic::class, $pet);
        $this->assertDatabaseHas('pets', ['name' => 'Whiskers']);
        $this->assertDatabaseHas('categories', ['name' => 'cat']);
    }
}
