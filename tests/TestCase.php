<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Support\InjectHeaders;
use Tests\Support\UriBuilder;
use Tests\Support\LoadAssets;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, InjectHeaders, UriBuilder, LoadAssets, MockeryPHPUnitIntegration;

    protected static $migrated = false;

    public function setUp(): void
    {
        parent::setUp();
        $this->prepareForTests();
    }

    private function prepareForTests()
    {
        if (static::$migrated) {
            return;
        }
        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
        static::$migrated = true;
    }

    protected function assertResponseStatus(TestResponse $response, int $status)
    {
        $state = (int) $response->status();
        $this->assertSame($status, $state, 'Unexpected status returned');
    }
}
