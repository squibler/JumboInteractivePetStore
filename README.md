# Jumbo Exercise

- Built on Laravel 5.8
- Tested with PHPUnit tests found in `tests/Unit` and `tests/Endpoints`
- Under test is Controllers and Business Logic classes

## Available Endpoints

| Method    | URI              | Name | Action                                     | Middleware |
| --------- | ---------------- | ---- | ------------------------------------------ | ---------- |
| POST      | api/v1/pet       |      | App\Http\Controllers\PetController@store   | api        |
| GET\|HEAD | api/v1/pet/{pet} |      | App\Http\Controllers\PetController@show    | api        |
| DELETE    | api/v1/pet/{pet} |      | App\Http\Controllers\PetController@destroy | api        |
| GET\|HEAD | api/v1/pets      |      | App\Http\Controllers\PetController@list    | api        |

## QA Test Results

> PHPStan has some issues with Laravel itself and there is nothing to backward check on
> which explians the 2 failues in this output

```bash
8/8 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%
```

|   | Action                       | Command                                              | Passed |
| - | ---------------------------- | ---------------------------------------------------- | ------ |
| 1 | CodeBeautifierCommand        | ./vendor/bin/phpcbf --report=json                    | YES    |
| 2 | ParallelLinterCommand        | ./vendor/bin/parallel-lint --exclude ./vendor .      | YES    |
| 3 | UnitTestCommand              | ./vendor/bin/phpunit                                 | YES    |
| 4 | StaticAnalysisCommand        | ./vendor/bin/phpstan analyse -l 4 app tests packages |  NO    |
| 5 | CodeSnifferCommand           | ./vendor/bin/phpcs --report=json                     | YES    |
| 6 | DocCheckCommand              | ./vendor/bin/php-doc-check --exclude=./vendor .      | YES    |
| 7 | BackwardCompatibilityCommand | ./vendor/bin/roave-backward-compatibility-check      |  NO    |
| 8 | SecurePackagesCommand        | php artisan security-check:now                       | YES    |

### PHPUnit

```bash
$ ./vendor/bin/phpunit
PHPUnit 7.5.8 by Sebastian Bergmann and contributors.

.........                                                           9 / 9 (100%)

Time: 1.43 seconds, Memory: 22.00 MB

OK (9 tests, 37 assertions)
```

### PHPCS (PSR2 Standards)

```bash
$ ./vendor/bin/phpcs
.................. 18 / 18 (100%)


Time: 151ms; Memory: 8MB
```